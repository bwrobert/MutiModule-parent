package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.demo.oneWithMany.course.read;

import java.util.List;
import java.util.Map;

import com.MutiModule.common.myPage.MyPageView;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.course.OneWithManyCourse;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.view.CourseStudentView;

/**
 * OneWithManyCourse 模块 读接口
 * @author alexgaoyh
 *
 */
public interface IOneWithManyCourseReadService {

	int selectCountByMap(Map<Object, Object> map);

	List<OneWithManyCourse> selectListByMap(Map<Object, Object> map);

	OneWithManyCourse selectByPrimaryKey(String id);
	
	/**
     * 获取分页实体信息部分
     * @param map	参数传递，封装部分过滤参数
     * @return
     */
    MyPageView<OneWithManyCourse> generateMyPageViewVO(Map<Object, Object> map);

    /**
     * 根据course 的过滤规则，查询出 一个班级下的所有学生信息
     * @param map
     * @return
     */
    List<CourseStudentView> selectCourseWithStudentByExample(Map<Object, Object> map);
    
    /**
     *  获取分页实体信息部分
     * @param map	参数过来，封装部分过滤参数
     * @return
     */
    MyPageView<CourseStudentView> generateMyPageViewVOWithStudent(Map<Object, Object> map);
    
}
