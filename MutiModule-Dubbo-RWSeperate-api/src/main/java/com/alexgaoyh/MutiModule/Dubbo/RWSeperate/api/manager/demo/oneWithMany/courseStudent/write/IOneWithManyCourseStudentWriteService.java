package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.demo.oneWithMany.courseStudent.write;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.courseStudent.OneWithManyCourseStudentKey;

/**
 * OneWithManyCourseStudent 模块 写接口
 * @author alexgaoyh
 *
 */
public interface IOneWithManyCourseStudentWriteService {

	int deleteByPrimaryKey(OneWithManyCourseStudentKey key);

	int insert(OneWithManyCourseStudentKey record);

	int insertSelective(OneWithManyCourseStudentKey record);

}
