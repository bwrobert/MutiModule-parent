package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.param.data.location.write;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.param.data.location.ParamDataLocation;

/**
 * ParamDataLocation 模块 写接口
 * @author alexgaoyh
 *
 */
public interface IParamDataLocationWriteService {

	int deleteByPrimaryKey(String id);

	int insert(ParamDataLocation record);

	int insertSelective(ParamDataLocation record);

	int updateByPrimaryKeySelective(ParamDataLocation record);

	int updateByPrimaryKey(ParamDataLocation record);
}
