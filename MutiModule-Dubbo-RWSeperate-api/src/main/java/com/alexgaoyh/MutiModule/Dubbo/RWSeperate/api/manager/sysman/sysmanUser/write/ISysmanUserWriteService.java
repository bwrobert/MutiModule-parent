package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanUser.write;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUser.SysmanUser;

/**
 * SysmanUser 模块 写接口
 * @author alexgaoyh
 *
 */
public interface ISysmanUserWriteService {

	int deleteByPrimaryKey(String id);

    int insert(SysmanUser record);

    int insertSelective(SysmanUser record);

    int updateByPrimaryKeySelective(SysmanUser record);

    int updateByPrimaryKey(SysmanUser record);
}
