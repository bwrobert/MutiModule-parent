package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.${packageName}.read;

import java.util.List;
import java.util.Map;

import com.MutiModule.common.myPage.MyPageView;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.${packageName}.${className};

/**
 * ${className} 模块 读接口
 * @author alexgaoyh
 *
 */
public interface I${className}ReadService {

	int selectCountByMap(Map<Object, Object> map);

	List<${className}> selectListByMap(Map<Object, Object> map);

	${className} selectByPrimaryKey(String id);
    
    /**
     * 获取分页实体信息部分
     * @param map	参数传递，封装部分过滤参数
     * @return
     */
    MyPageView<${className}> generateMyPageViewVO(Map<Object, Object> map);
    
}
