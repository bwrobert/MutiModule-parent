package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view;

import java.util.List;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.model.SqlDataAuthorityOperationModel;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.rule.SqlDataAuthorityOperationRule;

/**
 * 某个业务资源模块下配置的所有配置子模型；
 * 某一个 数据权限配置规则下所包含的所有配置子模型
 * 形如   班级资源配置，配置项下包含的所有 业务规则，比较 班主任权限配置项下能够查看此班级下的所有学生，某一种生活老师只能看到某种性别下的学生
 * 则 男生活老师下配置的子模型 为  sex = MALE	性别男
 * 则 女生活老师下配置的子模型为  sex = FEMALE	性别女
 * 此种配置子模型为一对多的配置模型
 * @author alexgaoyh
 *
 */
public class SqlDataAuthorityOperationView extends SqlDataAuthorityOperationRule{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2977438417420562876L;
	
	/**
	 * 某一个 数据权限配置规则下所包含的所有配置子模型
	 * 形如   班级资源配置，配置项下包含的所有 业务规则，比较 班主任权限配置项下能够查看此班级下的所有学生，某一种生活老师只能看到某种性别下的学生
	 * 则 男生活老师下配置的子模型 为  sex = MALE	性别男
	 * 则 女生活老师下配置的子模型为  sex = FEMALE	性别女
	 * 此种配置子模型为一对多的配置模型
	 */
	private List<SqlDataAuthorityOperationModel> items;

	public List<SqlDataAuthorityOperationModel> getItems() {
		return items;
	}

	public void setItems(List<SqlDataAuthorityOperationModel> items) {
		this.items = items;
	}
	
	
}
