package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.view;

import java.util.List;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.course.OneWithManyCourse;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.student.OneWithManyStudent;

/**
 * 	关联关系 一对多的view 实体类
 * @author alexgaoyh
 *
 */
public class CourseStudentView extends OneWithManyCourse{

	private List<OneWithManyStudent> studentList;

	public List<OneWithManyStudent> getStudentList() {
		return studentList;
	}

	public void setStudentList(List<OneWithManyStudent> studentList) {
		this.studentList = studentList;
	}
	
}
