package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.upload;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.upload.DemoAttachment;
import java.util.List;
import java.util.Map;

public interface DemoAttachmentMapper {
    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<DemoAttachment> selectListByMap(Map<Object, Object> map);

    int insert(DemoAttachment record);

    int insertSelective(DemoAttachment record);

    DemoAttachment selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(DemoAttachment record);

    int updateByPrimaryKey(DemoAttachment record);
}