package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.view;

import java.util.List;

import com.MutiModule.common.vo.ZTreeNodes;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.DictDictionary;

/**
 * 视图类 ，字典表 父子关系 视图类
 * 子类为 ZTreeNode 类
 * @author alexgaoyh
 *
 */
public class DictDictionaryWithZtreeNodeView extends DictDictionary{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6152754176887645113L;
	
	private List<ZTreeNodes> zTreeNodess;

	public List<ZTreeNodes> getzTreeNodess() {
		return zTreeNodess;
	}

	public void setzTreeNodess(List<ZTreeNodes> zTreeNodess) {
		this.zTreeNodess = zTreeNodess;
	}
	
	
}
