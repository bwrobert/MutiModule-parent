package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRole;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.MutiModule.common.vo.TreeNode;
import com.MutiModule.common.vo.ZTreeNodes;

public interface SysmanRoleMapper {
    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<SysmanRole> selectListByMap(Map<Object, Object> map);

    int insert(SysmanRole record);

    int insertSelective(SysmanRole record);

    SysmanRole selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(SysmanRole record);

    int updateByPrimaryKey(SysmanRole record);
    
    // alexgaoyh add begin

    /**
	 * 根据 后台用户 获取用户id下所包含的所有角色集合
	 * 
	 * @param id
	 *            用户id
	 * @return
	 */
	List<SysmanRole> selectBySysmanUserId(String id);
	
	List<TreeNode> selectTreeNodeBySysmanRoleId(String id);

	List<ZTreeNodes> selectAllRoleZTreeNode();

	List<SysmanRole> selectRoleListByUserId(String userId);

	/**
	 * 获取到所有 parent_id 为 null 的 资源集合
	 * 
	 * @return
	 */
	List<SysmanRole> selectSysmanRoleListByNullParentId();

	/**
	 * 根据当前用户 ID 与 当前资源的主键 id，获取到 用户包含的 权限集合
	 * 
	 * @param userId
	 * @param parentId
	 * @return
	 */
	List<SysmanRole> selectIconRoleByRoleIdAndParentId(@Param("roleId") String roleId,
			@Param("parentId") String parentId);
}