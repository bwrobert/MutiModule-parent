package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRoleResourceRel;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRoleResourceRel.SysmanRoleResourceRelKey;
import java.util.List;
import java.util.Map;

public interface SysmanRoleResourceRelMapper {
	
	int deleteByPrimaryKey(SysmanRoleResourceRelKey key);

	int selectCountByMap(Map<Object, Object> map);

	List<SysmanRoleResourceRelKey> selectListByMap(Map<Object, Object> map);

	int insert(SysmanRoleResourceRelKey record);

	int insertSelective(SysmanRoleResourceRelKey record);

	/**
	 * 根据 角色id ，删除此角色下所有资源
	 * 
	 * @param sysmanRoleId
	 *            角色id
	 * @return
	 */
	int deleteBySysmanRoleId(String sysmanRoleId);
}