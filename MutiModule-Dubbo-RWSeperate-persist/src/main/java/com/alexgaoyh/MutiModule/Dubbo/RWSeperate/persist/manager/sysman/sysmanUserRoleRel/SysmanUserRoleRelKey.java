package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUserRoleRel;

import com.MutiModule.common.mybatis.base.BaseEntity;
import java.io.Serializable;

public class SysmanUserRoleRelKey extends BaseEntity implements Serializable {
    private String sysmanUserId;

    private String sysmanRoleId;

    private static final long serialVersionUID = 1L;

    public String getSysmanUserId() {
        return sysmanUserId;
    }

    public void setSysmanUserId(String sysmanUserId) {
        this.sysmanUserId = sysmanUserId;
    }

    public String getSysmanRoleId() {
        return sysmanRoleId;
    }

    public void setSysmanRoleId(String sysmanRoleId) {
        this.sysmanRoleId = sysmanRoleId;
    }
}