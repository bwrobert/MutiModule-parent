package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.courseStudent;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.student.OneWithManyStudentMapper;

public class DemoOneWithManyStudentTest {

	private OneWithManyStudentMapper mapper;

	//@Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (OneWithManyStudentMapper) ctx.getBean( "oneWithManyStudentMapper" );
        
    }
	
	//@Test
	public void deleteStudentListByClassIdTest() {
		
		int deleteCount = mapper.deleteStudentListByClassId("11126620571706368");
		System.out.println(deleteCount);
	}
}
