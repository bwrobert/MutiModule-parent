package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.upload;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.common.vo.mybatis.pagination.Page;

public class UploadAttachmentTest {

	private DemoAttachmentMapper mapper;

	// @Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (DemoAttachmentMapper) ctx.getBean( "demoAttachmentMapper" );
        
    }
	
	// @Test
	public void selectCourseWithStudentByExampleTest() {
		Map<Object , Object> map = new HashMap<Object, Object>();
		//map.put("id", 22L);
		Page page = new Page(0, 10);
		map.put("page", page);
		
		int count = mapper.selectCountByMap(map);
		List<DemoAttachment> list = mapper.selectListByMap(map);
		System.out.println("count = " + count);
		System.out.println("list.size() = " + list.size());
	}
	
}
