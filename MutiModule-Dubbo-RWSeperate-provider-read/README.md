#20151112
	Dubbo 服务提供者，读模块，数据库方面可以设定为丛库，只能进行读操作； 配置文件部分事务控制中仅仅只能read-only;
	
	添加 Druid的监控功能； 仅仅添加了部分监控功能，根据业务可以进行其他方面的配置；
	
#20160225
	此项目为webapp项目，如果在使用过程中，想把这个项目打包为可执行jar ，按照如下步奏：
		1： 修改pom.xml 文件的packaging参数，由war 改为 jar
		2： 取消掉 maven-shade-plugin 插件的注释
		3： mvn install 
	执行完毕之后，生成 *-jar-with-dependencies.jar 文件，使用  java -jar *-jar-with-dependencies.jar 即可启动；
	
#20160308
	Dubbo-consumer Dubbo-provider-read Dubbo-provider-write 三个模块：
		抽离 配置文件 xml 内部的 
			<dubbo:registry protocol="zookeeper" address="192.168.2.211:2181" />
	部分 到 Dubbo-api 模块中，减少重复代码；一个地方修改即可；
	
#20160421
	Dubbo-RWSeperate-* 模块：
			将 sys_*  后台 RABC 相关的表结构都进行处理，移除 Example 相关的表结构，改为自定义的通用方法；
				本地扩展mybatis-generator 插件，自定义插件解决基础功能处理；
				后续有新的业务逻辑，则重新扩展此自定义插件；
				
#20160629
	Dubbo-RWSeperate-provider-read 模块： 
		增加AOP 注解，测试解决 数据权限的问题：
			注意 com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.dataAuthority.annotation 包下的类文件；
				一个是针对 method（方法），说明这个方法需要进行数据权限的设定，需要在执行前增加额外的数据参数的增加；
				一个是针对class  (类)  ， 说明这个类     需要增加额外的数据权限的设定，其中className 用来存储真正关联的实体类的绝对路径；
				
		通过使用 AOP 方式，在执行service方法之前，动态把相关配置的数据权限加载到入参参数中；
			使用方式，需要在需要植入数据权限的service实现方法部分加上 DataAuthorityMethodAnnotation 注解； 在此service实现类部分加上 DataAuthorityClassAnnotation 注解（同时加上 className 部分（实体类绝对路径））；
			增加相关的 SqlModelVO 类注释，详见 com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.dataAuthority.aspect.test.vo.DictDictionaryVOUtilss 类文件，可以通过 DictDictionaryTest 单元测试部分进行测试检查； 