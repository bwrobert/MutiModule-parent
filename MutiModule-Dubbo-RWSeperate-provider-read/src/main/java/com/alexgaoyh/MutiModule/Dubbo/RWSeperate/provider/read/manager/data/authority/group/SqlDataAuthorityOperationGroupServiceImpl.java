package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.manager.data.authority.group;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.myPage.util.MyPageViewUtilss;
import com.MutiModule.common.mybatis.annotation.data.authority.DataAuthorityClassAnnotation;
import com.MutiModule.common.mybatis.annotation.data.authority.DataAuthorityMethodAnnotation;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.data.authority.group.read.ISqlDataAuthorityOperationGroupReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.group.SqlDataAuthorityOperationGroup;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.group.SqlDataAuthorityOperationGroupMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view.SqlDataAuthorityOperationGroupWithRuleView;

@DataAuthorityClassAnnotation(className = "com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.group.SqlDataAuthorityOperationGroup")
@Service(value = "sqlDataAuthorityOperationGroupService")
public class SqlDataAuthorityOperationGroupServiceImpl implements ISqlDataAuthorityOperationGroupReadService{
	
	@Resource(name = "sqlDataAuthorityOperationGroupMapper")
	private SqlDataAuthorityOperationGroupMapper mapper;

	@Override
	public int selectCountByMap(Map<Object, Object> map) {
		return mapper.selectCountByMap(map);
	}

	@Override
	public List<SqlDataAuthorityOperationGroup> selectListByMap(Map<Object, Object> map) {
		return mapper.selectListByMap(map);
	}

	@Override
	public SqlDataAuthorityOperationGroup selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}
	
	@Override
	public MyPageView<SqlDataAuthorityOperationGroup> generateMyPageViewVO(Map<Object, Object> map) {
		
		int _totalCount = mapper.selectCountByMap(map);
		
		List<SqlDataAuthorityOperationGroup> _list = mapper.selectListByMap(map);
		
		int recordPerPage = 10;
		int currentPage = 1;
		if(map.get("page") != null) {
			Object _pageObj = map.get("page");
			if(_pageObj instanceof com.MutiModule.common.vo.mybatis.pagination.Page) {
				com.MutiModule.common.vo.mybatis.pagination.Page _page = (com.MutiModule.common.vo.mybatis.pagination.Page)_pageObj;
				currentPage = _page.getPageNo();
				recordPerPage = _page.getLength();
			}
		}
		MyPageView<SqlDataAuthorityOperationGroup> pageView = MyPageViewUtilss.generaterMyPageView(recordPerPage, currentPage, _totalCount, _list);
		
		return pageView;
		
	}
	
	@DataAuthorityMethodAnnotation
	@Override
	public MyPageView<SqlDataAuthorityOperationGroup> generateMyPageViewVO(String currentSysmanUserLoginId,
			Map<Object, Object> map) {
		return this.generateMyPageViewVO(map);
	}


	@Override
	public SqlDataAuthorityOperationGroupWithRuleView selectGroupWithRuleByPrimaryKey(String id) {
		return mapper.selectGroupWithRuleViewById(id);
	}

}
