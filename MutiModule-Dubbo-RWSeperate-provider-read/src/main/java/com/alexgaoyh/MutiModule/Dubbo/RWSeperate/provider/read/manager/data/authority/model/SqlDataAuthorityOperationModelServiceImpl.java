package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.manager.data.authority.model;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.myPage.util.MyPageViewUtilss;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.data.authority.model.read.ISqlDataAuthorityOperationModelReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.model.SqlDataAuthorityOperationModel;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.model.SqlDataAuthorityOperationModelMapper;

@Service(value = "sqlDataAuthorityOperationModelService")
public class SqlDataAuthorityOperationModelServiceImpl implements ISqlDataAuthorityOperationModelReadService{
	
	@Resource(name = "sqlDataAuthorityOperationModelMapper")
	private SqlDataAuthorityOperationModelMapper mapper;

	@Override
	public int selectCountByMap(Map<Object, Object> map) {
		return mapper.selectCountByMap(map);
	}

	@Override
	public List<SqlDataAuthorityOperationModel> selectListByMap(Map<Object, Object> map) {
		return mapper.selectListByMap(map);
	}

	@Override
	public SqlDataAuthorityOperationModel selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}
	
	@Override
	public MyPageView<SqlDataAuthorityOperationModel> generateMyPageViewVO(Map<Object, Object> map) {
		
		int _totalCount = mapper.selectCountByMap(map);
		
		List<SqlDataAuthorityOperationModel> _list = mapper.selectListByMap(map);
		
		int recordPerPage = 10;
		int currentPage = 1;
		if(map.get("page") != null) {
			Object _pageObj = map.get("page");
			if(_pageObj instanceof com.MutiModule.common.vo.mybatis.pagination.Page) {
				com.MutiModule.common.vo.mybatis.pagination.Page _page = (com.MutiModule.common.vo.mybatis.pagination.Page)_pageObj;
				currentPage = _page.getPageNo();
				recordPerPage = _page.getLength();
			}
		}
		MyPageView<SqlDataAuthorityOperationModel> pageView = MyPageViewUtilss.generaterMyPageView(recordPerPage, currentPage, _totalCount, _list);
		
		return pageView;
		
	}

}
