package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.manager.sysman.sysmanRole;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.myPage.util.MyPageViewUtilss;
import com.MutiModule.common.vo.TreeNode;
import com.MutiModule.common.vo.ZTreeNodes;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRole.read.ISysmanRoleReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource.SysmanResource;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRole.SysmanRole;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRole.SysmanRoleMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.util.tree.TreeConvert;

@Service(value = "sysmanRoleService")
public class SysmanRoleServiceImpl implements ISysmanRoleReadService{
	
	@Resource(name = "sysmanRoleMapper")
	private SysmanRoleMapper mapper;

	@Override
	public int selectCountByMap(Map<Object, Object> map) {
		return mapper.selectCountByMap(map);
	}

	@Override
	public List<SysmanRole> selectListByMap(Map<Object, Object> map) {
		return mapper.selectListByMap(map);
	}

	@Override
	public SysmanRole selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public MyPageView<SysmanRole> generateMyPageViewVO(Map<Object, Object> map) {
		int _totalCount = mapper.selectCountByMap(map);
		
		List<SysmanRole> _list = mapper.selectListByMap(map);
		
		int recordPerPage = 10;
		int currentPage = 1;
		if(map.get("page") != null) {
			Object _pageObj = map.get("page");
			if(_pageObj instanceof com.MutiModule.common.vo.mybatis.pagination.Page) {
				com.MutiModule.common.vo.mybatis.pagination.Page _page = (com.MutiModule.common.vo.mybatis.pagination.Page)_pageObj;
				currentPage = _page.getPageNo();
				recordPerPage = _page.getLength();
			}
		}
		MyPageView<SysmanRole> pageView = MyPageViewUtilss.generaterMyPageView(recordPerPage, currentPage, _totalCount, _list);
		
		return pageView;
	}

	@Override
	public List<TreeNode> selectTreeNodeBySysmanRoleId(String id) {
		return mapper.selectTreeNodeBySysmanRoleId(id);
	}

	@Override
	public List<ZTreeNodes> selectAllResourceZTreeNode() {
		return mapper.selectAllRoleZTreeNode();
	}

	@Override
	public List<SysmanRole> selectRoleListByUserId(String userId) {
		return mapper.selectRoleListByUserId(userId);
	}

	@Override
	public List<SysmanRole> selectSysmanRoleListByNullParentId() {
		return mapper.selectSysmanRoleListByNullParentId();
	}

	@Override
	public List<SysmanRole> selectIconRoleByRoleIdAndParentId(String roleId, String parentId) {
		return mapper.selectIconRoleByRoleIdAndParentId(roleId, parentId);
	}

	@Override
	public List<String> selectParentIdsByPrimaryKey(String id, List<String> list) {
		SysmanRole sRole = mapper.selectByPrimaryKey(id);
		if(sRole != null) {
			if(null != sRole.getParentId()) {
				list.add(0, sRole.getParentId());
				selectParentIdsByPrimaryKey(sRole.getParentId(), list);
			}
		}
		return list;
	}

	@Override
	public List<TreeNode> selectTreeNodeListByUserId(String userId) {
		List<SysmanRole> allNodesList = mapper.selectRoleListByUserId(userId);
		
		List<SysmanRole> rootNodesList = TreeConvert.getRootNodesRoleList(allNodesList);
		
		List<TreeNode> treeNodeList = TreeConvert.convertSysmanRoleListToTreeNodeList(rootNodesList, allNodesList);
		
		return treeNodeList;
	}


}
