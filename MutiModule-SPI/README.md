#20151028
	SPI 全称为 (Service Provider Interface) ,是JDK内置的一种服务提供发现机制。 目前有不少框架用它来做服务的扩展发现， 
	简单来说，它就是一种动态替换发现的机制， 举个例子来说， 有个接口，想运行时动态的给它添加实现，你只需要添加一个实现，而后，把新加的实现，描述给JDK知道就行啦（通过改一个文本文件即可）
	目前Dubbo框架就基于SPI机制提供扩展功能。
	
	
	└── src
	│   └── com.MutiModule
	│       		└── SPI.test
	│           			├── HelloInterface.java
	│           			├── impl
	│           			│   ├── ImageHello.java
	│           			│   └── TextHello.java
	│           	└── SPIMain.java
	└── META-INF
	    └── services
	        └── com.MutiModule.SPI.test.HelloInterface
	        
    ***
    	META-INF/services 文件夹放置在Classpath下面
    		目录下放置一个配置文件，文件名使用扩展的接口全名，文件内部为接口实现类，分行符分隔，使用UTF-8接口
	
	
	提供给服务提供厂商与扩展框架功能的开发者使用的接口。
		在我们日常开发的时候都是对问题进行抽象成Api然后就提供各种Api的实现，
		这些Api的实现都是封装与我们的Jar中或框架中的虽然当我们想要提供一种Api新实现时可以不修改原来代码只需实现该Api就可以提供Api的新实现，
		但我们还是生成新Jar或框架（虽然可以通过在代码里扫描某个目录已加载Api的新实现，但这不是Java的机制，只是hack方法），
		而通过Java SPI机制我们就可以在不修改Jar包或框架的时候为Api提供新实现。
		
		 很多框架都使用了java的SPI机制，如java.sql.Driver的SPI实现（mysql驱动、oracle驱动等）、common-logging的日志接口实现、dubbo的扩展实现等等框架；