package com.MutiModule.common.dict;

/**
 * 定时任务字典类
 * 	字典表常量信息集合
 * @author alexgaoyh
 *
 */
public class ScheduleDict {

	public static final boolean DEBUG = true;

	/**
	 * TIMESTAMP 一天的长度  毫秒  1秒=1000毫秒(ms)
	 */
	public static final long DAY_TIMESTAMP = 24L * 3600L * 1000L;

	public static final long A2A_WAIT_PAY_DAY = 3 * DAY_TIMESTAMP;
	public static final long A2A_WAIT_SEND_DAY = 5 * DAY_TIMESTAMP;
	public static final long A2A_WAIT_REC_DAY = 7 * DAY_TIMESTAMP;

	public static final long A2M_WAIT_PAY_DAY = 3 * DAY_TIMESTAMP;
	public static final long A2M_WAIT_SEND_DAY = 5 * DAY_TIMESTAMP;
	public static final long A2M_WAIT_REC_DAY = 7 * DAY_TIMESTAMP;

	public static final long M2A_WAIT_PAY_DAY = 3 * DAY_TIMESTAMP;
	public static final long M2A_WAIT_SEND_DAY = 5 * DAY_TIMESTAMP;
	public static final long M2A_WAIT_REC_DAY = 7 * DAY_TIMESTAMP;

	public static final long M2M_WAIT_PAY_DAY = 3 * DAY_TIMESTAMP;
	public static final long M2M_WAIT_SEND_DAY = 5 * DAY_TIMESTAMP;
	public static final long M2M_WAIT_REC_DAY = 7 * DAY_TIMESTAMP;
	
}
