package com.MutiModule.common.mybatis.base;

import java.io.Serializable;

import com.MutiModule.common.vo.enums.DeleteFlagEnum;

/**
 * mybatis 部分，抽离公共基础model
 * @author alexgaoyh
 *
 */
public class BaseEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 主键ID， 主键部分
	 */
	private String id;

	/**
	 * 删除标示
	 */
	private DeleteFlagEnum deleteFlag;

	/**
	 * 创建时间
	 */
	private String createTime;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public DeleteFlagEnum getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(DeleteFlagEnum deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

}
