package com.MutiModule.common.un;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.MutiModule.common.utils.ExcelReader;
import com.MutiModule.common.utils.FileUtilss;

public class ExcelUtilssTest1 {

	public static int getLengthTest(String filePath, int column1, int column2) {
		Set<String>  set = new HashSet<String>();
    	Sheet sheet = ExcelReader.readExcelSheet(filePath, 1);
    	int rowNum = sheet.getLastRowNum();
    	for (int i = 1; i <= rowNum; i++) {
    		Row row = sheet.getRow(i);
    		String _str = ExcelReader.getCellFormatValue(row.getCell((short) column1)).trim() + ExcelReader.getCellFormatValue(row.getCell((short) column2)).trim();
    		set.add(_str);
    	}
    	return set.size();
	}
	
	/**
	 *  单日不重复的订单数  -----
	 *  TODO
	 */
	public static void readFileFolderList(String filePath, int column1, int column2) {
		
		List<String> list = FileUtilss.listFileNames(filePath);
		for(String _str : list) {
			System.out.println(_str  + "  :" + getLengthTest(filePath + File.separator + _str, column1, column2));
		}
	}
	
	public static void main(String[] args) {
		//readFileFolderList("C:\\Users\\lenovo\\Desktop\\12.22订单汇总\\12.22订单汇总");
		
		readFileFolderList(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]));
	}
	
}
