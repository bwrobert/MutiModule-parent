package com.MutiModule.common.utils.btree.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.MutiModule.common.utils.btree.BTree;

public class BTreeIntegerTest {

	private static BTree<Integer> tree = new BTree<Integer>();

	private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

	public static void main(String args[]) throws IOException {

		System.out.println("test balanced tree operations");
		System.out.println("*****************************");

		String input;
		Integer value;

		do {
			input = stringInput("please select: [i]nsert, [q]preOrder, [z]inOrder, [h]postOrder, [l]getHeight, [e]xit");
			switch (input.charAt(0)) {
			case 'i':
				value = Integer.parseInt(stringInput("insert: "), 10);
				if (tree.isMember(value)) {
					System.out.println("value " + value + " already in tree");
				} else {
					tree.insert(value);
				}
				break;
			case 'd':
				value = Integer.parseInt(stringInput("delete: "), 10);
				if (tree.isMember(value)) {
					tree.delete(value);
				} else {
					System.out.println(value + " not found in tree");
				}
				break;
			case 'q':
				System.out.println(tree.preOrder());
				break;
			case 'z':
				System.out.println(tree.inOrder());
				break;
			case 'h':
				System.out.println(tree.postOrder());
				break;
			case 'l':
				System.out.println(tree.getHeight());
				break;
			}
			
		} while ((input.charAt(0) != 'e'));
	}

	private static String stringInput(String inputRequest) throws IOException {
		System.out.println(inputRequest);
		return reader.readLine();
	}
}
