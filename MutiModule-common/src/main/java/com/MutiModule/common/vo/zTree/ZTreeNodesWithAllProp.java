package com.MutiModule.common.vo.zTree;

import java.io.Serializable;
import java.util.List;

/**
 * ztree 包含所有属性
 * @author alexgaoyh
 *
 */
public class ZTreeNodesWithAllProp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7667462207818914574L;

	private String id;
	
	private String pId;
	
	private boolean isParent;
	
	private String name;
	
	private int level;
	
	private String tId;
	
	private String parentTId;
	
	private boolean open;
	
	private boolean zAsync;
	
	private boolean isFirstNode;
	
	private boolean isLastNode;
	
	private boolean isAjaxing;
	
	private boolean checked;
	
	private boolean checkedOld;
	
	private boolean nocheck;
	
	private boolean chkDisabled;
	
	private boolean halfCheck;
	
	private int check_Child_State;
	
	private boolean check_Focus;
	
	private boolean isHover;
	
	private boolean editNameFlag;
	
	private List<ZTreeNodesWithAllProp> children;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getpId() {
		return pId;
	}

	public void setpId(String pId) {
		this.pId = pId;
	}

	public boolean isParent() {
		return isParent;
	}

	public void setParent(boolean isParent) {
		this.isParent = isParent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String gettId() {
		return tId;
	}

	public void settId(String tId) {
		this.tId = tId;
	}

	public String getParentTId() {
		return parentTId;
	}

	public void setParentTId(String parentTId) {
		this.parentTId = parentTId;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public boolean iszAsync() {
		return zAsync;
	}

	public void setzAsync(boolean zAsync) {
		this.zAsync = zAsync;
	}

	public boolean isFirstNode() {
		return isFirstNode;
	}

	public void setFirstNode(boolean isFirstNode) {
		this.isFirstNode = isFirstNode;
	}

	public boolean isLastNode() {
		return isLastNode;
	}

	public void setLastNode(boolean isLastNode) {
		this.isLastNode = isLastNode;
	}

	public boolean isAjaxing() {
		return isAjaxing;
	}

	public void setAjaxing(boolean isAjaxing) {
		this.isAjaxing = isAjaxing;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isCheckedOld() {
		return checkedOld;
	}

	public void setCheckedOld(boolean checkedOld) {
		this.checkedOld = checkedOld;
	}

	public boolean isNocheck() {
		return nocheck;
	}

	public void setNocheck(boolean nocheck) {
		this.nocheck = nocheck;
	}

	public boolean isChkDisabled() {
		return chkDisabled;
	}

	public void setChkDisabled(boolean chkDisabled) {
		this.chkDisabled = chkDisabled;
	}

	public boolean isHalfCheck() {
		return halfCheck;
	}

	public void setHalfCheck(boolean halfCheck) {
		this.halfCheck = halfCheck;
	}

	public int getCheck_Child_State() {
		return check_Child_State;
	}

	public void setCheck_Child_State(int check_Child_State) {
		this.check_Child_State = check_Child_State;
	}

	public boolean isCheck_Focus() {
		return check_Focus;
	}

	public void setCheck_Focus(boolean check_Focus) {
		this.check_Focus = check_Focus;
	}

	public boolean isHover() {
		return isHover;
	}

	public void setHover(boolean isHover) {
		this.isHover = isHover;
	}

	public boolean isEditNameFlag() {
		return editNameFlag;
	}

	public void setEditNameFlag(boolean editNameFlag) {
		this.editNameFlag = editNameFlag;
	}

	public List<ZTreeNodesWithAllProp> getChildren() {
		return children;
	}

	public void setChildren(List<ZTreeNodesWithAllProp> children) {
		this.children = children;
	}
	
}
