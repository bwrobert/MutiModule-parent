package com.MutiModule.common.asyncCallback;

/**
 * 请求者
 * @author alexgaoyh
 *
 */
public class Ask implements CallBack{
	
	private Answer answer;
	
	public Ask(Answer answer) {
		this.answer = answer;
	}
	
	public void askQuestion(final String question){  
        //这里用一个线程就是异步，  
        new Thread(new Runnable() {  
            @Override  
            public void run() {  
            	answer.executeMessage(Ask.this, question);   
            }  
        }).start();  
          
        System.out.println("请求者发出一个异步请求之后，接着执行请求者的事情!");  
    }  
  
	@Override
	public void solve(String result) {
		System.out.println("返回值为：--->" + result);
	}

}
