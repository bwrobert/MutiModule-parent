package com.MutiModule.common.utils.file;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.MutiModule.common.utils.FileUtilss;

public class FileUtilssTest {

	private final static List<String> showAllFiles(File dir, List<String> filePath) throws Exception {
		File[] fs = dir.listFiles();
		for (int i = 0; i < fs.length; i++) {
			filePath.add(fs[i].getAbsolutePath());
			if (fs[i].isDirectory()) {
				try {
					showAllFiles(fs[i], filePath);
				} catch (Exception e) {
				}
			}
		}
		return filePath;
	}

	/**
	 * 读取某个文件夹路径下的所有文件，并将这些文件的内容写入到同一个文件里面
	 */
	// @Test
	public void readFileFolderPathAndWriteToOneFile() {
		
		try {
			File file = new File("F:\\MyEclipse 8.5\\WorkspacesMyEclipse 8.5\\ccjf\\");
			List<String> filePath = new ArrayList<String>();
			List<String> fileList = showAllFiles(file, filePath);
			if (fileList != null) {
				for (String string : fileList) {
					if(string.endsWith(".java")) {
						String fileContent = FileUtilss.readString(string, "utf-8");
						FileUtilss.appendString(fileContent, "e://123456.txt", "utf-8");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
