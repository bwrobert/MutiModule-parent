package com.MutiModule.common.xml2json;

import org.junit.Test;

import com.MutiModule.common.utils.XML2JSONUtilss;

public class XML2JSONTest {

	public static int PRETTY_PRINT_INDENT_FACTOR = 4;
    public static String TEST_XML_STRING =
        "<OUTPUT><E_FLAG>0000</E_FLAG><E_FLAG_DES>订单退款核算已完成</E_FLAG_DES><E_KBETR>440.00</E_KBETR><E_KBEWR>0.00</E_KBEWR></OUTPUT>"
    		+ 
        		"<TABLES><T_ITEMS><item><MANDT>300</MANDT><POSNR>000010</POSNR><MATNR>000000000006020001</MATNR><KWMENG>34.000</KWMENG><VRKME1>KG</VRKME1><LFIMG>12.000</LFIMG><REKMG>22.000</REKMG><VRKME2>KG</VRKME2><KBETR>440.00</KBETR><NETPR>440.00</NETPR><NETWR>0.00</NETWR><WAERK>CNY</WAERK></item></T_ITEMS></TABLES>";
    
    //@Test
    public void xml2json() {
    	System.out.println(XML2JSONUtilss.xml2json(TEST_XML_STRING));
    }
}
