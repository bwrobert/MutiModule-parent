package com.alexgaoyh.MutiModule.persist.base.location;

import java.io.Serializable;
import java.util.Date;

public class BaseLocation implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.id
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.deleteFlag
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private Integer deleteFlag;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.createTime
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.nameCN
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private String nameCN;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.nameEN
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private String nameEN;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.code
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private String code;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.inputCode
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private String inputCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.languageType
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private String languageType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.level
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private Integer level;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.longName
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private String longName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.parentId
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private Integer parentId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.zipCode
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private String zipCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.shortName
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private String shortName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.orderNo
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private String orderNo;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.substr1
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private String substr1;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.substr2
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private String substr2;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.subdouble1
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private Double subdouble1;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.subdouble2
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private Double subdouble2;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.subdate1
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private Date subdate1;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.subdate2
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private Date subdate2;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.creator
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private String creator;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.createName
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private String createName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.modifier
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private String modifier;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.modifyName
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private String modifyName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.modifyTime
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private Date modifyTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column base_location.isOften
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private String isOften;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table base_location
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.id
     *
     * @return the value of base_location.id
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.id
     *
     * @param id the value for base_location.id
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.deleteFlag
     *
     * @return the value of base_location.deleteFlag
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public Integer getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.deleteFlag
     *
     * @param deleteFlag the value for base_location.deleteFlag
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setDeleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.createTime
     *
     * @return the value of base_location.createTime
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.createTime
     *
     * @param createTime the value for base_location.createTime
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.nameCN
     *
     * @return the value of base_location.nameCN
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public String getNameCN() {
        return nameCN;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.nameCN
     *
     * @param nameCN the value for base_location.nameCN
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setNameCN(String nameCN) {
        this.nameCN = nameCN;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.nameEN
     *
     * @return the value of base_location.nameEN
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public String getNameEN() {
        return nameEN;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.nameEN
     *
     * @param nameEN the value for base_location.nameEN
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setNameEN(String nameEN) {
        this.nameEN = nameEN;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.code
     *
     * @return the value of base_location.code
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public String getCode() {
        return code;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.code
     *
     * @param code the value for base_location.code
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.inputCode
     *
     * @return the value of base_location.inputCode
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public String getInputCode() {
        return inputCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.inputCode
     *
     * @param inputCode the value for base_location.inputCode
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setInputCode(String inputCode) {
        this.inputCode = inputCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.languageType
     *
     * @return the value of base_location.languageType
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public String getLanguageType() {
        return languageType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.languageType
     *
     * @param languageType the value for base_location.languageType
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setLanguageType(String languageType) {
        this.languageType = languageType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.level
     *
     * @return the value of base_location.level
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.level
     *
     * @param level the value for base_location.level
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.longName
     *
     * @return the value of base_location.longName
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public String getLongName() {
        return longName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.longName
     *
     * @param longName the value for base_location.longName
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setLongName(String longName) {
        this.longName = longName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.parentId
     *
     * @return the value of base_location.parentId
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public Integer getParentId() {
        return parentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.parentId
     *
     * @param parentId the value for base_location.parentId
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.zipCode
     *
     * @return the value of base_location.zipCode
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.zipCode
     *
     * @param zipCode the value for base_location.zipCode
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.shortName
     *
     * @return the value of base_location.shortName
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.shortName
     *
     * @param shortName the value for base_location.shortName
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.orderNo
     *
     * @return the value of base_location.orderNo
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.orderNo
     *
     * @param orderNo the value for base_location.orderNo
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.substr1
     *
     * @return the value of base_location.substr1
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public String getSubstr1() {
        return substr1;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.substr1
     *
     * @param substr1 the value for base_location.substr1
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setSubstr1(String substr1) {
        this.substr1 = substr1;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.substr2
     *
     * @return the value of base_location.substr2
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public String getSubstr2() {
        return substr2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.substr2
     *
     * @param substr2 the value for base_location.substr2
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setSubstr2(String substr2) {
        this.substr2 = substr2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.subdouble1
     *
     * @return the value of base_location.subdouble1
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public Double getSubdouble1() {
        return subdouble1;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.subdouble1
     *
     * @param subdouble1 the value for base_location.subdouble1
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setSubdouble1(Double subdouble1) {
        this.subdouble1 = subdouble1;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.subdouble2
     *
     * @return the value of base_location.subdouble2
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public Double getSubdouble2() {
        return subdouble2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.subdouble2
     *
     * @param subdouble2 the value for base_location.subdouble2
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setSubdouble2(Double subdouble2) {
        this.subdouble2 = subdouble2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.subdate1
     *
     * @return the value of base_location.subdate1
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public Date getSubdate1() {
        return subdate1;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.subdate1
     *
     * @param subdate1 the value for base_location.subdate1
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setSubdate1(Date subdate1) {
        this.subdate1 = subdate1;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.subdate2
     *
     * @return the value of base_location.subdate2
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public Date getSubdate2() {
        return subdate2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.subdate2
     *
     * @param subdate2 the value for base_location.subdate2
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setSubdate2(Date subdate2) {
        this.subdate2 = subdate2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.creator
     *
     * @return the value of base_location.creator
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public String getCreator() {
        return creator;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.creator
     *
     * @param creator the value for base_location.creator
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.createName
     *
     * @return the value of base_location.createName
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public String getCreateName() {
        return createName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.createName
     *
     * @param createName the value for base_location.createName
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setCreateName(String createName) {
        this.createName = createName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.modifier
     *
     * @return the value of base_location.modifier
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.modifier
     *
     * @param modifier the value for base_location.modifier
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.modifyName
     *
     * @return the value of base_location.modifyName
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public String getModifyName() {
        return modifyName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.modifyName
     *
     * @param modifyName the value for base_location.modifyName
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setModifyName(String modifyName) {
        this.modifyName = modifyName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.modifyTime
     *
     * @return the value of base_location.modifyTime
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.modifyTime
     *
     * @param modifyTime the value for base_location.modifyTime
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column base_location.isOften
     *
     * @return the value of base_location.isOften
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public String getIsOften() {
        return isOften;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column base_location.isOften
     *
     * @param isOften the value for base_location.isOften
     *
     * @mbggenerated Sun Jul 26 08:28:46 CST 2015
     */
    public void setIsOften(String isOften) {
        this.isOften = isOften;
    }
}