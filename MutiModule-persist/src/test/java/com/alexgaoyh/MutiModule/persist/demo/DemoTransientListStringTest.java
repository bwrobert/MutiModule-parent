package com.alexgaoyh.MutiModule.persist.demo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.common.utils.JSONUtilss;
import com.MutiModule.common.vo.enums.DeleteFlagEnum;

/**
 * 测试 Transient 注解，Demo.java实体类中增加 Transient 注解的方法，getJsonStr
 * 这样保存至name字段的值为json字符串，之后selectByPrimaryKey 方法取出的entity实体类，
 * 可以调用 getJsonStr 方法将对应的字符串转换为json化之前的数据格式
 * 这样，不需要修改格外的代码，只需要在实体类中增加 Transient 注解的方法即可
 * @author alexgaoyh
 *
 */
public class DemoTransientListStringTest {


	private DemoMapper mapper;
	
	//@Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (DemoMapper) ctx.getBean( "demoMapper" );
        
    }
	
	//@Test
	public void testInsert() {
		
		try {
			Demo entity = new Demo();
			entity.setDeleteFlag(DeleteFlagEnum.NORMAL);
			entity.setCreateTime(new Date());
			
			List<String> list = new ArrayList<String>();
			list.add("alexgaoyh1");
			list.add("alexgaoyh2");
			list.add("alexgaoyh3");
			
			entity.setName(JSONUtilss.toJSon(list));
			
			mapper.insert(entity);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//@Test
	public void testSelect() {
		try {
			Demo entity = mapper.selectByPrimaryKey(55);
			/*List<String> list= entity.getJsonStr();
			for(String str : list) {
				System.out.println(str);
			}*/
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
