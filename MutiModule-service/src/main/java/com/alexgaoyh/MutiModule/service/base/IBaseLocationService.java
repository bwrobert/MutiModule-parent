package com.alexgaoyh.MutiModule.service.base;

import java.util.List;
import java.util.Map;

import com.MutiModule.common.vo.Pagination;
import com.MutiModule.common.vo.TreeNode;
import com.MutiModule.common.vo.ZTreeNode;
import com.alexgaoyh.MutiModule.persist.base.location.BaseLocation;
import com.alexgaoyh.MutiModule.persist.base.location.BaseLocationExample;

/**
 * 
 * @desc IBaseLocationService e接口
 *
 * @author alexgaoyh
 */
public interface IBaseLocationService {

	/**
	 * 插入操作
	 * @param BaseLocation baseLocation 插入操作实体类数据
	 */
	public void insert(BaseLocation baseLocation);
	
	/**
	 * 插入操作，根据实体类的相关参数，匹配插入数据
	 * @param BaseLocation baseLocation 插入操作实体类数据
	 */
	public void insertSelective(BaseLocation baseLocation);
	
	/**
	 * 根据id获取实体信息
	 * @param id
	 */
	public BaseLocation selectByPrimaryKey(Integer id);
	
	/**
	 * 根据主键id 有选择的更新实体信息
	 * @param BaseLocation baseLocation 根据主键id，有选择的更新操作实体类数据
	 */
	public int updateByPrimaryKeySelective(BaseLocation baseLocation);
	
	/**
	 * 根据入参信息（包含分页页码，过滤条件）返回符合条件的数据信息
	 * @param example 过滤条件
	 * @return 此条件下共有多少条匹配数据
	 */
	public int countByExample(BaseLocationExample example);
	
	/**
	 * 根据入参信息（包含分页页码，过滤条件）返回符合条件的数据信息
	 * @param example 过滤条件
	 * @return 此条件下返回的数据list集合
	 */
	public List<BaseLocation> selectByExample(BaseLocationExample example);
	
	/**
	 * 根据入参信息（包含分页页码，过滤条件）返回符合条件的数据信息
	 * @param BaseLocationExample 过滤条件
	 * @return 分页信息
	 */
	Pagination<BaseLocation> getPanigationByRowBounds(BaseLocationExample example);
	
	/**
	 * 根据example查询出相关的数据信息，并将这些数据信息进行更新，更新参数如record
	   @param record 更新的值
	 * @param example 过滤条件
	 * @return 符合条件的数据条数
	 */
	public int updateByExampleSelective(BaseLocation record, BaseLocationExample example);
	
	/**
	 * 根据example删除出相关的数据集，并对其进行删除操作
	 * @param example 过滤条件
	 * @return 符合条件的删除数据条数
	 */
	public int deleteByExample(BaseLocationExample example);
	
	/**
	 * 根据ids数组，逻辑删除对象
	 * @param deleteFlag
	 * @param ids
	 * @return
	 */
	public int deleteLogicByIds(Integer deleteFlag, Integer[] ids);
	
	/**
     * 根据当前id。获取这个id对应资源的属性结构数据
     * @param id 表中的主键id
     * @return 对应主键id下的属性结构
     */
    List<TreeNode> selectTreeNodeById(Integer id);
    
    
    /**
   	 * 根据parentid返回符合条件的孩子节点集合
   	 * @param parentId	父亲id
   	 * @return
   	 */
   	List<BaseLocation> selectListByParentId(Integer parentId);
   	
   	/**
   	 * 查询当前表结构中，parentId is null 的集合字段
   	 * @return	list<BaseLocation>
   	 */
   	List<BaseLocation> selectTopByParentId();
   	
   	/**
   	 * 根据父节点id，获取到这个节点下所有的ZtreeNode节点集合
   	 * @return
   	 */
   	List<ZTreeNode> selectZTreeNodeListByParentId(Integer parentId);
   	
   	/**
   	 * 返回省市区三级功能对应的 拼音缩写， 汉字缩写， ids 集合
   	 * 形如： 
   	 * @return
   	 */
   	List<Map<String, Object>> getRegionHashMap();
   	
   	/**
   	 * 获取地区名称
   	 * @return
   	 */
   	List<Map<String, Object>> getRegionNameCN();
}
