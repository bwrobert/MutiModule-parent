package com.alexgaoyh.MutiModule.service.demo;

import java.util.List;

import com.MutiModule.common.vo.Pagination;
import com.alexgaoyh.MutiModule.persist.demo.Demo;
import com.alexgaoyh.MutiModule.persist.demo.DemoExample;

/**
 * 
 * @desc IDemoService e接口
 *
 * @author alexgaoyh
 */
public interface IDemoService {

	/**
	 * 插入操作
	 * @param Demo demo 插入操作实体类数据
	 */
	public int insert(Demo demo);
	
	/**
	 * 插入操作，根据实体类的相关参数，匹配插入数据
	 * @param Demo demo 插入操作实体类数据
	 */
	public void insertSelective(Demo demo);
	
	/**
	 * 根据id获取实体信息
	 * @param id
	 */
	public Demo selectByPrimaryKey(Integer id);
	
	/**
	 * 根据主键id 有选择的更新实体信息
	 * @param Demo demo 根据主键id，有选择的更新操作实体类数据
	 */
	public int updateByPrimaryKeySelective(Demo demo);
	
	/**
	 * 根据入参信息（包含分页页码，过滤条件）返回符合条件的数据信息
	 * @param example 过滤条件
	 * @return 此条件下共有多少条匹配数据
	 */
	public int countByExample(DemoExample example);
	
	/**
	 * 根据入参信息（包含分页页码，过滤条件）返回符合条件的数据信息
	 * @param example 过滤条件
	 * @return 此条件下返回的数据list集合
	 */
	public List<Demo> selectByExample(DemoExample example);
	
	/**
	 * 根据入参信息（包含分页页码，过滤条件）返回符合条件的数据信息
	 * @param DemoExample 过滤条件
	 * @return 分页信息
	 */
	Pagination<Demo> getPanigationByRowBounds(DemoExample exampleForCount, DemoExample exampleForList);
	
	/**
	 * 根据example查询出相关的数据信息，并将这些数据信息进行更新，更新参数如record
	   @param record 更新的值
	 * @param example 过滤条件
	 * @return 符合条件的数据条数
	 */
	public int updateByExampleSelective(Demo record, DemoExample example);
	
	/**
	 * 根据example删除出相关的数据集，并对其进行删除操作
	 * @param example 过滤条件
	 * @return 符合条件的删除数据条数
	 */
	public int deleteByExample(DemoExample example);
	
	/**
	 * 根据ids数组，逻辑删除对象
	 * @param deleteFlag
	 * @param ids
	 * @return
	 */
	public int deleteLogicByIds(Integer deleteFlag, Integer[] ids);
	
}
