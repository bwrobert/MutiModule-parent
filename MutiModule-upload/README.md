#20150817
	upload 模块-资源上传统一调用模块：
		使用 https://github.com/fex-team/webuploader/ 解决相关的前端上传功能；

		可以访问  /html/upload.html 进行本地测试，使用二进制文件流对上传的图片进行解析操作(解决移动端的问题)
	
		增加 CORSFilter ， 解决跨域问题；（web.xml中配置Filter）
		
		增加FileUploadServlet 处理图片上传相关
			
			（图片上传使用二进制流进行操作，） 修改webuploader.js 文件中 sendAsBinary 字段设置（false改为true）；
					https://github.com/fex-team/webuploader/issues/185  (解决移动端不兼容的情况)；
					
#20151220
	upload 模块-资源上传统一调用模块：
		图片上传+文件上传 已完成，样式部分可以根据业务进一步调整（用户体验度不高）；
			现阶段 FileUploadServlet/PicUploadServlet 区分文件上传和图片上传的两种方式；
				可根据业务特点，确定是否合并；
		uploadFile.html（文件上传Demo） 
		uploadPic.html （图片上传Demo）
		
	增加跨域控制，不允许 cors.properties 文件内配置的链接外的Origin对资源进行请求访问
		
		