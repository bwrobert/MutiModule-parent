<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Manager</title>

	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/jquery-easyui-1.4/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/jquery-easyui-1.4/themes/icon.css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery-v1.11.3.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-easyui-1.4/jquery.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-easyui-1.4/jquery.easyui.min.js"></script>
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui-extend/easyui_dataGrid_blank_extend.js"></script>
	
</head>
<body class="easyui-layout" >
	<div data-options="region:'center',split:false">
		
		<div id="toolbar-1">
			<a href="javascript:history.go(-1);" class="easyui-linkbutton" iconCls="icon-back" plain="true">返回</a>
			<a href="javascript:void(0);" class="easyui-linkbutton save" iconCls="icon-save" plain="true">保存</a>
		</div>
		
		<div id="dlg-1">
			<jsp:include page="form.jsp"></jsp:include>
		</div>
		
	</div>
	
	<script type="text/javascript">
		var context_ = '${context_}';
		var templateUrl = '${moduleName}';
		$( function() {
			
			var dg1 = new DataGridBlankEasyui(context_, 1 , templateUrl , undefined);
			
			//dg1.init();
			
		});
	</script>
	
</body>
</html>