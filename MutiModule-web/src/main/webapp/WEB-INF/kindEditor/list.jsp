<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ page isELIgnored="false"%>   
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!doctype html>
<html>
<head>
	<meta charset="utf-8" />
	<title>KindEditor JSP</title>
	
	<jsp:include page="../common/adminCommon.jsp"></jsp:include>	
	
	<link rel="stylesheet" href="${pageContext.request.contextPath}/kindeditor/themes/default/default.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/kindeditor/plugins/code/prettify.css" />
	<script charset="utf-8" src="${pageContext.request.contextPath}/kindeditor/kindeditor-all.js"></script>
	<script charset="utf-8" src="${pageContext.request.contextPath}/kindeditor/lang/zh-CN.js"></script>
	<script charset="utf-8" src="${pageContext.request.contextPath}/kindeditor/plugins/code/prettify.js"></script>
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/kindeditor-extend/kindeditor-extend.js"></script>
	
	<script>
		var sysmanUserId = '${sysmanUser.id}';
		
		$( function() {
			
			var editor = KindEditorExtend.initEditor({
				contextURL : context_,
				contextPath : "admin",
				detailPath : sysmanUserId,
				kindEditorId : "content1",
			});
			
			console.log(editor.html("sdafasdf"));
			
		});
		
	</script>
</head>
<body>
		<textarea name="content1" cols="100" rows="8" style="width:700px;height:200px;visibility:hidden;"></textarea>
</html>